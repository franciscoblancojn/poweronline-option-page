<?php
add_action( 'admin_menu', 'CRNAD_option_page_add_menu' );
function CRNAD_option_page_add_menu() {
	add_menu_page(
		'CRNAD_option', // page <title>Title</title>
		'Administrador de Cronograma', // menu link text
		'manage_options', // capability to access the page
		'CRNAD_option-slug', // page URL slug
		'CRNAD_function_option_page', // callback function /w content
		'dashicons-grid-view', // menu icon
		5 // priority
	);
    add_submenu_page(
        'CRNAD_option-slug',
        "Coach Rating",
        "Coach Rating",
		'manage_options', // capability to access the page
		'CRNAD_option-slug-coach-rating', // page URL slug
        'CRNAD_option_page_Coach_Rating',
    );
}
add_action( 'admin_init',  'CRNAD_option_page_register_setting' );
 
function CRNAD_option_page_register_setting(){
 
	register_setting(
		'CRNAD_option_settings', // settings group name
		'input_CRNAD_option_settings', // option name
		'sanitize_text_field' // sanitization function
	);
	register_setting(
		'CRNAD_option_settings', // settings group name
		'URL_suscripcion', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		'CRNAD_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'CRNAD_option-slug' // page slug
	);
 
	add_settings_field(
		'input_CRNAD_option_settings',
		'input_CRNAD_option_settings',
		'CRNAD_option_text_field_html', // function which prints the field
		'CRNAD_option-slug', // page slug
		'CRNAD_option_settings_section_id', // section ID
		array( 
			'label_for' => 'input_CRNAD_option_settings',
			'class' => 'misha-class', // for <tr> element
		)
    );
	add_settings_field(
		'URL_suscripcion',
		'URL_suscripcion',
		'CRNAD_URL_suscripcion', // function which prints the field
		'CRNAD_option-slug', // page slug
		'CRNAD_option_settings_section_id', // section ID
		array( 
			'label_for' => 'URL Suscripcion',
			//'class' => 'misha-class', // for <tr> element
		)
    );
}
function CRNAD_option_text_field_html(){
 
	$text = get_option( 'input_CRNAD_option_settings' );
 
	printf(
		'<input type="text" id="input_CRNAD_option_settings" name="input_CRNAD_option_settings" value="%s" />',
		esc_attr( $text )
	);
 
}
function CRNAD_URL_suscripcion(){
 
	$text = get_option( 'URL_suscripcion' );
 
	printf(
		'<input type="text" id="URL_suscripcion" name="URL_suscripcion" value="%s" />',
		esc_attr( $text )
	);
 
}
function CRNAD_function_option_page(){
    $dias = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
    ?> 
    <div class="wrap">
	    <h1>Cronograma</h1>
        <div class="content">
            <table>
                <thead>
                    <tr>
                        <th>Hora</th>
                        <?php
                        foreach ($dias as $key => $value) {
                            ?>
                            <th><?=$value?></th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i=CRNAD_minHora; $i < CRNAD_maxHora; $i++) { 
                        ?>
                        <tr class="tr_clase">
                            <td class="number_hora"><?=$i?>:00</td>
                            <?php
                            foreach ($dias as $key => $value) {
                                ?>
                                <td>
                                    <div class="content_clases">
                                        <button
                                        id="add_<?=$value."_".$i?>"
                                        ele="<?=$value."_".$i?>"
                                        class="button button-primary add_btn"
                                        onclick="add_clase(this)"
                                        >Add</button>
                                        <input 
                                        id="<?=$value."_".$i?>"
                                        name="<?=$value."_".$i?>"
                                        dia="<?=$value?>"
                                        hora="<?=$i?>"
                                        type="text"
                                        class="input_class"
                                        >
                                    </div>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr class="tr_clase">
                            <td class="number_hora"><?=$i?>:30</td>
                            <?php
                            foreach ($dias as $key => $value) {
                                ?>
                                <td>
                                    <div class="content_clases">
                                        <button
                                        id="add_<?=$value."_".$i."_mm"?>"
                                        ele="<?=$value."_".$i."_mm"?>"
                                        class="button button-primary add_btn"
                                        onclick="add_clase(this)"
                                        >Add</button>
                                        <input 
                                        id="<?=$value."_".$i."_mm"?>"
                                        name="<?=$value."_".$i."_mm"?>"
                                        dia="<?=$value?>"
                                        hora="<?=$i."_mm"?>"
                                        type="text"
                                        class="input_class"
                                        >
                                    </div>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <p>
            Para mostrar el Cronograma use el shortcode [CRNAD_Cronograma]
            <br>
            o use el widget de elementor CRNAD
        </p>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'CRNAD_option_settings' ); 
                do_settings_sections( 'CRNAD_option-slug' ); 
                submit_button();
            ?>
	    </form>
        <style>
            .misha-class,.input_class{
                display:none;
            }
            .number_hora{
                text-align:right;
            }
            .content_clases{
                display:flex;
                flex-wrap:wrap;
                width:240px;
            }
            .content_clases > *{
                width:100%;
                order:5;
            }
            .content_clases .clase{
                order:1;
                display:flex;
                flex-wrap:wrap;
            }
            .content_clases .coach,
            .content_clases .link{
                width:100%;
            }
            .tr_clase{
                margin-bottom:20px;
            }
            table{
                border-collapse: collapse;
            }
            td,tr,th{
                border:2px solid;
            }
            td{
                padding:10px 15px;
            }
        </style>
        <script>
            input = document.getElementById('input_CRNAD_option_settings')
            submit = document.getElementById('submit')
            value = {}
            function load_value(){
                value = input.value
                if(value == ""){
                    <?php
                    $horas = "{";
                    for ($i=CRNAD_minHora; $i < CRNAD_maxHora; $i++) { 
                        $horas .= '"'.$i.'":"",';
                        $horas .= '"'.$i.'_mm":""';
                        if($i != CRNAD_maxHora - 1){
                            $horas .= ",";
                        }
                    }
                    $horas .= "}";
                    $v = "{";
                    foreach ($dias as $key => $value) {
                        $v .= '"'.$value.'":'.$horas;
                        if($key != 6){
                            $v .= ",";
                        }
                    }
                    $v .= "}";
                    ?>
                    value = `<?=$v?>`
                }
                value = JSON.parse(value)
                for (const [dia, horas] of Object.entries(value)) {
                    for (const [hora, link] of Object.entries(horas)) {
                        e = document.getElementById(`${dia}_${hora}`)
                        e.value = link
                    }
                }
            }
            load_value()
            function save_value() {
                for (const [dia, horas] of Object.entries(value)) {
                    for (const [hora, link] of Object.entries(horas)) {
                        e = document.getElementById(`${dia}_${hora}`)
                        value[dia][hora] = e.value
                    }
                }
                input.value = JSON.stringify(value)
            }
            submit.onclick = function() {
                save_value()
            }
            function add_clase(e , v = null){
                v = v || {
                    coach : "",
                    link : ""
                }
                count = e.getAttribute("count") || 0
                id = e.getAttribute("ele")
                ele = e.parentElement
                var newele = document.createElement("p");
                newele.classList.add("clase")
                newele.innerHTML += `
                    <input
                    id="coach_${id}_${count}"
                    class="coach"
                    onchange="change_class(this)"
                    type="text"
                    placeholder="Coach"
                    ele="${id}"
                    value="${v.coach}"
                    />
                    <input
                    id="link_${id}_${count}"
                    class="link"
                    onchange="change_class(this)"
                    type="text"
                    placeholder="Url Clase"
                    ele="${id}"
                    value="${v.link}"
                    />
                    <button
                    id="delete_${id}_${count}"
                    class="button button-secundary delete"
                    onclick="delete_class(this)"
                    ele="${id}"
                    >
                        Delete
                    </button>
                `
                ele.appendChild(newele);
                count++
                e.setAttribute("count",count)
            }
            function change_class(e){
                save_class(e)
            }
            function delete_class(e){
                ele = e.parentElement
                ele.outerHTML = ""
                save_class(e)
            }
            function save_class(e){
                id = e.getAttribute("ele")
                ele = document.getElementById(id)
                coachs = document.documentElement.querySelectorAll(`[id^="coach_${id}"]`)
                links = document.documentElement.querySelectorAll(`[id^="link_${id}"]`)
                value_clases = []
                for (i = 0; i < coachs.length; i++) {
                    value_clases[i] = {
                        'coach' : coachs[i].value,
                        'link' : links[i].value,
                    };
                }
                ele.value = JSON.stringify(value_clases)
                console.log(value_clases)
            } 
            function load_class(){
                clases = document.documentElement.querySelectorAll(".input_class")
                add_btn = document.documentElement.querySelectorAll(".add_btn")
                for ( i = 0; i < clases.length; i++) {
                    if(clases[i].value !=""){
                        c = JSON.parse(clases[i].value)
                        for ( j = 0; j < c.length; j++) {
                            add_clase(add_btn[i],c[j])
                        }
                    }
                }
            } 
            load_class() 
        </script>
    </div>
    <?php
}

function CRNAD_option_page_Coach_Rating()
{
    $data = get_option("CRNAD_Coach_Rating");
    if($data == null || $data == "" || $data == undefine){
        $data = "[]";
    }
    $data = json_decode($data,true);
    ?>

    <h1>Coach Rating</h1>
        <table class="table-coach-rating">
            <thead>
                <tr>
                    <th>
                        <div class="Coach">
                            Coash
                        </div>
                        
                    </th>
                    <th>
                        <div class="rating">
                            Rating
                        </div>
                        
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($data as $key => $info) {
                        ?>
                        <tr>
                            
                                <td>
                                    <div class="Coach">
                                         <?=$info["coach"]?>
                                    </div>
                                </td>
                            
                            
                                <td>
                                    <div class="rating">
                                        <?=$info["rating"]?>
                                    </div>
                                </td>
                            
                           
                            
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
        </table>

        <style>
            .rating{
                padding: 10px 50px;
                border :1px solid #000;
                border-radius:10px;
                text-align:center;
            }
            .Coach{
                padding: 10px 50px;
                border :1px solid #000; 
                border-radius:10px;
                text-align:center;
            }
            .table-coach-rating {
                width:100%;
            }
        </style>

    <?php
}



