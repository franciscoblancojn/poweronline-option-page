<?php

function CRNAD_Cronograma()
{
    if(get_current_user_id() == 0){
        ?>
        <script>
            window.location="/"
        </script>
        <?php
        exit;
    }


    date_default_timezone_set("America/Bogota");

    $cronograma = get_option( 'input_CRNAD_option_settings' );
    $cronograma = json_decode($cronograma,true);

	
    $dias = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
    $dia = $dias[date("N") - 1];
    
    
    $hora = date("H");
    $minutos = date("i") + CRNAD_timedown;
    if($minutos >= 60){
        $minutos = 0;
        $hore++;
    }
    $minutos = intval($minutos/30) * 30;

    $min_id = $hora.(($minutos == 30)?"_mm":"");
    
	
	
	$clases = $cronograma[$dia][$min_id];
    
	
	if(!$clases){
		return "Horario no disponible";
	}
    $clases = json_decode($clases,true);
    
    $array_coach = [];
    foreach ($cronograma[$dia] as $key => $value) {
        if($value == ""){
            $value = "[]";
        }
        $value = json_decode($value,true);
        
        
        

        for ($i=0; $i < count($value); $i++) { 
            $array_coach[$key][] = $value[$i]['coach'];
        }
        

       
        
    }
    ?>
    <style>
        .form_cronograma{
            max-width:400px;
            margin:auto;
            --radius : 10px;
            --width-span : 100px;
        }
        .form_cronograma .input_select{
            width: 100%;
            background-color: #ffffff;
            border-radius: var(--radius ,5px);
            display: block;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            display:flex;
            margin-bottom:20px;
        }
        .form_cronograma .input_select span{
            background:gainsboro;
            color:#818182;
            font-size:20px;
            border-top-left-radius:var(--radius ,5px);
            border-bottom-left-radius:var(--radius ,5px);
            font-weight:600;
            padding: 10px;
            display: inline-block;
            width:var(--width-span );
        }
        .form_cronograma .input_select select{
            display: inline-block;
            border:0;
            padding: 10px;
            padding-right:30px;
            font-size:20px;
            width:calc(100% - var(--width-span ));
            border-top-right-radius:var(--radius ,5px);
            border-bottom-right-radius:var(--radius ,5px);
            background:#fff;
            color:#818181;
            outline:none;
        }
        .form_cronograma #hora[date_a="am"] [date_a="pm"],
        .form_cronograma #hora[date_a="pm"] [date_a="am"]{
            display:none;
        }
        .form_cronograma .btn_ingresar.btn_ingresar.btn_ingresar.btn_ingresar{
            padding: 3px 10px;
            padding-left: 50px;
            border:0;
            border-radius: 5px;
            background: #e30521;
            text-transform: uppercase;
            font-size: 25px;
            position:relative;
            color:#fff;
            overflow:hidden;
            outline:none;
        }
        .form_cronograma .btn_ingresar.btn_ingresar.btn_ingresar.btn_ingresar:hover{
            color: #333333;
        }
        .form_cronograma .dashicons-lightning.dashicons-lightning.dashicons-lightning{
            background: #bc0522;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 40px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .form_cronograma .dashicons-lightning:before{
            content:"";
            margin: auto;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            width:20px;
            height: 20px;
            display: block;
            background-image: url("data:image/svg+xml;charset=utf8,%3C?xml version='1.0' encoding='utf-8'?%3E%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 670 719' style='enable-background:new 0 0 670 719;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0{fill:%23F2F2F2;} %3C/style%3E%3Cg%3E%3Cpath class='st0' d='M236.2,683.6c-2.6,9.4-0.8,10.2,4.1,1.7l230.6-394.8c5.1-8.5,1-15.6-8.8-15.6l-97.8-0.7 c-9.8-0.1-14.6-7.4-10.7-16.4l95.4-213.9c3.9-9-1-16.4-10.7-16.6l-78-0.5c-9.9-0.1-21,7.2-24.9,16.2L195.2,361.2 c-4,9,1,16.4,10.6,16.6l95.9,0.8c9.7,0.1,15.7,7.9,13.1,17.3L236.2,683.6z'/%3E%3C/g%3E%3C/svg%3E");
        }
		
        .form_cronograma .separacion_crnad{
            display:block;
            height:100px;
        }
        .content_relot{
            color:white;
            margin-bottom:20px;
        }
		.lightning{
			width:60%;
		}
		.valoracion {
			display:flex;
    position: relative;
    overflow: hidden;
    display: flex;
			padding: 10px;
    border: 2px solid #00E2D7;
    width: 280px;
			    
    flex-direction: row-reverse;
}
		
.valoracion input {
    position: absolute;
    top: -100px;
}


.valoracion label {
    float: left;
    color: #c1b8b8;
    font-size: 30px; 
	text-align:center;
	margin:5px;
}

.valoracion label:hover,
.valoracion label:hover ~ label,
.valoracion input:checked ~ label {
    color: #00E2D7;
	background:#00E2D7;
	border-radius:100%;
}
		
    </style>
    <div class="content_relot">
        Hora estándar de Colombia
        <div id="relot">
        </div>
    </div>
    <script>
		const user_id = "<?=get_current_user_id()?>";
        relot = document.getElementById('relot')
        hora_S = <?=date("H")?>;
        minutos = <?=date("i")?>;
        segundos = <?=date("s")?>;

        relot.innerHTML = `${hora_S}:${minutos}:${segundos}`
        ticRelot = () => {
            segundos++
            if(segundos>=60){
                segundos = 0;
                minutos++
            }
            if(minutos>=60){
                minutos = 0;
                hora_S++
            }
            if(hora_S>=24){
                hora_S = 0;
            }
            relot.innerHTML = `${hora_S}:${minutos}:${segundos}`
            setTimeout(() => {
                ticRelot()
            }, 1000);
        }
        ticRelot();
		getPuntuation = () =>{
			const r = document.querySelector(`[name="estrellas"]:checked`)
			const value = r?.value ?? 0
			return value
		}

		getDataSendPuntuation = () =>{
			return {
				coach:document.getElementById("coach").value,
				user_id,
				puntuatio:getPuntuation()
			}
		}
		const onSendData = async () => {
            const url = "<?=CRNAD_url?>";
			const data = getDataSendPuntuation()
            

            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify(data);

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };
            try {
                const respond = await fetch(`${url}saveRatingCoach.php`, requestOptions)

                const result = await respond.json()

                    alert("Tu puntuacion a sido enviada") 
            } catch (error) {
                alert("Error Puntuacion") 
            }
		}

		const onDetectSendData = () => {
			const r = document.querySelectorAll(`[name="estrellas"]`)
			for (var i = 0; i < r.length; i++) {
				r[i].addEventListener("click",onSendData)
			}
		}

		window.addEventListener("load",onDetectSendData)
    </script>
    <div class="form_cronograma">
		
        <label class="input_select">
            <span>
                Jornada
            </span>
            <select 
            name="horario" 
            id="horario"
            >
                <option value="am" <?=(date("a")=="am")?"selected":"";?> >Mañana</option>
                <option value="pm" <?=(date("a")=="pm")?"selected":"";?> >Tarde</option>
            </select>
        </label>
        <label class="input_select">
            <span>
                Hora
            </span>
            <select 
            name="hora" 
            id="hora"
            date_a="<?=date("a")?>"
            >
            <?php
            for ($i=CRNAD_minHora; $i < CRNAD_maxHora; $i++) { 
                $i_hora = $i;
                $date_a = "am";
                if($i>12){
                    $i_hora -=12;
                }
                if($i>=12){
                    $date_a = "pm";
                }
                ?>
                <option date_a="<?=$date_a?>" hora="<?=$i?>" min="<?=$minutos?>" value="<?=$i?>" <?=($hora == $i && $minutos == 0)?"selected":"";?>>
                    <?=$i_hora?>:00
                </option>
                <option date_a="<?=$date_a?>" hora="<?=$i?>" min="<?=$minutos?>" value="<?=$i?>_mm" <?=($hora == $i && $minutos == 30)?"selected":"";?>>
                    <?=$i_hora?>:30
                </option>
                <?php
            }
            ?>
            </select>
        </label>
        <label class="input_select">
            <span>
                Coach
            </span>
            <select 
            name="coach" 
            id="coach"
            >
            </select>
        </label>
		
			 
			<div class="valoracion">
    <input id="radio1" type="radio" name="estrellas" value="5"><!--
--><label for="radio1">
			 <img class="lightning" src="https://poweronline.com.co/wp-content/uploads/2022/10/thunder.png"/>
			 
			 
			 </label><!--
    --><input id="radio2" type="radio" name="estrellas" value="4"><!--
    --><label for="radio2"><img class="lightning" src="https://poweronline.com.co/wp-content/uploads/2022/10/thunder.png"/></label><!--
    --><input id="radio3" type="radio" name="estrellas" value="3"><!--
    --><label for="radio3"><img class="lightning" src="https://poweronline.com.co/wp-content/uploads/2022/10/thunder.png"/></label><!--
    --><input id="radio4" type="radio" name="estrellas" value="2"><!--
    --><label for="radio4"><img class="lightning" src="https://poweronline.com.co/wp-content/uploads/2022/10/thunder.png"/></label><!--
    --><input id="radio5" type="radio" name="estrellas" value="1"><!--
    --><label for="radio5"><img class="lightning" src="https://poweronline.com.co/wp-content/uploads/2022/10/thunder.png"/></label>
  </div> 
		
        <p id="respondHTML" style="color: white;"></p>
        <button id="btn_ingresar" class="btn_ingresar">
            <span class="icon_form dashicons dashicons-lightning"></span>
            Ingresar
        </button>
		
		<div class="recodtr">
			<span>
			recuerda que para acceder a la clase 
			es 5 minutos antes y 5 minutos depués
			de cada hora
			</span>
		</div>
		<a href="https://api.whatsapp.com/send?phone=573204094372&text=Hola%20%F0%9F%91%8B%F0%9F%8F%BB%20,tengo%20un%20problema%20con%20mi%20cuenta%20me%20puedes%20ayudar">
		<button class="btn_whatsapp">
			
				Contáctanos <br>
				<h2 class="buzon">
				🤳🏻		
				</h2>
				<span> Pagos y servicios</span>
			
        </button>
			</a>
		<a href="https://www.instagram.com/powerclub_official/">
		<button class="btn_instagram">
			
				📸 Instagram
			
        </button>
		</a>
    </div>
    <script>
        var hora = document.getElementById('hora')
        var coach = document.getElementById('coach')
        var horario = document.getElementById('horario')
        var btn_ingresar = document.getElementById('btn_ingresar')
        var respondHTML = document.getElementById('respondHTML')

        horario.onchange = function(){
            hora.setAttribute("date_a",horario.value)
        }
        btn_ingresar.onclick = function(){
            data = {
                coach : coach.value,
                hora : hora.value,
            }
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify(data)

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("<?=plugin_dir_url( __FILE__ )."action_clases.php"?>", requestOptions)
            .then(response => response.text())
            .then(result => respondResult(result))
            .catch(error => console.log('error', error));

           
        }
        hora.onchange = () => {
            load_select_coach()
        }
        var respondResult = (result) => {
            console.log(result);
            result = JSON.parse(result)
            if(result.status == "error"){
                console.log(result)
                link = "<?=get_option('URL_suscripcion')?>"
                respondHTML.innerHTML = `
                    ${result.msj}
                    <br>
                    <a href="${link}" target="_blank">Contactanos</a>
                `
                //alert(result.msj)
                if("noRegister" == result.typeError){
                    window.location="/"
                }
                if("noSuscription" == result.typeError){
                    console.log("noSuscription");
                    //window.open(link,"blank")
                }
            }else{
                link = result.data.link
                respondHTML.innerHTML = `
                    <h1 style="color:#fff;">
                        Tu clase esta <a href="${link}" target="_blank">aqui</a>
                    </h1>
                `
                window.open(link,"blank")
            }
        }
        const CRNAD_timedown = <?=CRNAD_timedown?>;
        const CRNAD_timeup = <?=CRNAD_timeup?>;

        var reloadPage = () => {
            var min = (new Date()).getMinutes()
            var hora = (new Date()).getHours()
            console.log("min")
            if( 
                min >= 60 - CRNAD_timedown ||
                (min >= 30 - CRNAD_timedown && min <= 30 + CRNAD_timeup) ||
                min <= CRNAD_timeup
            ){
                min = (min >= 30)?"_mm":""
                horaSelected = document.documentElement.querySelector("#hora [selected]")

                if(horaSelected!=null && horaSelected!=undefined){
                    horaSelected = horaSelected.value
                    value = `${hora}${min}`

                    if(horaSelected != value){
                        //window.location.reload()
                    }
                }
            }
            setTimeout(() => {
                reloadPage()
            }, 1000 * 60);
        }
        <?=(CRNAD_minHora < $hora && CRNAD_maxHora > $hora)?"reloadPage()":"";?>
        
        var load_select_coach = (x = null ) => {
            const select_coach = <?=json_encode($array_coach)?>;
            console.log(select_coach);
            var value = select_coach[hora.value];

            if(x!=null){
                value = select_coach[x];
            }
            var options = `
                <option value="">No disponible</option>
            `
            if(value != null && value != undefined){
                options = ""

                for (var i = 0; i < value.length; i++) {
                    options += `
                        <option value="${value[i]}">${value[i]}</option>
                    `
                }

            }else{

            }
            coach.innerHTML = options
        }
        load_select_coach()
    </script>
    <?php
}
add_shortcode('CRNAD_Cronograma', 'CRNAD_Cronograma');
function CRNAD_table()
{
    $cronograma = get_option( 'input_CRNAD_option_settings' );
    $cronograma = json_decode($cronograma,true);
    $dias = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
    ?>
    <div class="conograma">
        <table>
            <thead>
                <tr>
                    <th>Hora</th>
                    <?php
                    foreach ($dias as $key => $value) {
                        ?>
                        <th><?=$value?></th>
                        <?php
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                for ($i=CRNAD_minHora; $i < CRNAD_maxHora; $i++) { 
                    ?>
                    <tr>
                        <td class="number_hora"><?=$i?>:00</td>
                        <?php
                        foreach ($dias as $key => $value) {
                            $link = $cronograma[$value][$i];
                            ?>
                            <td>
                                <a href="<?=$link?>">
                                <?=$link?>
                                </a>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td class="number_hora"><?=$i?>:30</td>
                        <?php
                        foreach ($dias as $key => $value) {
                            $link = $cronograma[$value][$i."_mm"];
                            ?>
                            <td>
                                <a href="<?=$link?>">
                                <?=$link?>
                                </a>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}
add_shortcode('CRNAD_table', 'CRNAD_table');
function CRNAD_regeneratePassword()
{
    $user_ids = get_users(
        array(
            'role'    => 'Customer',
            'returns' => 'ids',
        )
    );
    for ($i=0; $i < count($user_ids); $i++) { 
        $user_id = $user_ids[$i]->ID;
        echo "<pre>";
        var_dump($user_id);
        echo "</pre>";
        wp_set_password( "123456789" , $user_id );
    }
}
//add_shortcode('CRNAD_regeneratePassword', 'CRNAD_regeneratePassword');
function CRNAD_createNewUsers()
{
    $newUser = array(
        array("ALEJANDRA GUTIERREZ MOLINA","ALEJAGM2116@GMAIL.COM","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("JULIETH PAOLA GUTIERREZ","JULIETHGUTIERREZ607@GMAIL.COM","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("LORENA CADAVID ","lorenaosorio621@gmail.com","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("ELISA OLANO VELEZ","ELISA.OLANOV@GMAIL.COM","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("DIANA CRISTINA GOMEZ ","diana_8629@hotmail.com ","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("KATERINE OSORIO MAZO","Ktyosorio94@gmail.com","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("MARCELA PANIAGUA ","mfranco1985@gmail.com","TRANSFORMACION VIRTUAL","26/02/2020"),
        array("Deicy Machado","daissy2927@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Camila Segura","segurar.camila@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Gloria elsy Tejada Restrepo","gloriaelsy8@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Shirley Vasquez","shirleyvasquez189@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("luz aide tejada restrepo","zulaide2006@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Karen Piedrahíta","ktapiecor_11@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Denise Naranjo","dbrigitten@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Juan Carlos Zuluaga Jaramillo","jczj73@yahoo.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Yurani Marín Cardona","yurani8822@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Wilson Andrés Tamayo Palacio","antamayo1026@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("GUSTAVO BABILONIA LUNA","babilonialunamd@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Yessica Macías Altamiranda","yessicamacias2194@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Laura Castrillón Mercado","castrillonlaura01@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Elizabeth Orozco Mesa","eorozcom@sura.com.co","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Laura Alzate Hernández","alzatelaura98@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Angie marquez","angiedayana16@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Johanna Milena Castro Escobar","johannacastro0817@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("MARCELA ARENAS","arenasmarcela@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Maria Teresa Hincapie Palacio","tere11a1@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Laura palacio","Lalap_68@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("lemia romam","lamiaroman@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("SANDRA CRISTINA AGUDELO","sandracagudelo@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("ÁNGELA MARÍA GIL","Angelagil2h@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("NATALIA ORJUELA","NATALIA.ORJUELA@HOTMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("ANDREA CAROLINA SANCHEZ BUITRAGO","ANCASABU20@YAHOO.ES","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Valentina Álvarez","valen-alvarez701@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Juliana Beltrán Sierra","beltransierrajuliana@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Laura Bedoya T","lbedoyat95@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("JOHANA FARFAN MEJIA","JOHANAFM88@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("ADRIANA ALDERETE","ADRIANATATIANA1@HOTMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("BRAYAN NICOLAS DUQUE SOTO","BRAYAN.DUQUE.SOTO1@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("ANDREA STELLA PARADA GARCIA","PQ17NOV@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("MATTHEW GOMEZ MARTINEZ","MATT.GOMEZ310@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Manuela Ortega","manusyn@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Laura Cristina Ruiz Villa","lauracruizv@gmil.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("PAOLA FONTALVO","jaha12@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Stefani Jimenez Potes","stefajimenez87@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Paola Andrea Chamorro Zuluaga","paoa674@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Cristin Alonso Montoya Lopera","calvino_32@yahoo.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Ana Maria Gonzalez Angel","anagonzalezangel@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("SILVIA PATRICIA VARGAS","s_vargas29@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("PAULA HOYOS","pahoyosorozco@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("CINTHYA JOHANA JIMENEZ","cinthyajoa100@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("LIZETH RODRIGUEZ SANCHEZ","lizrodrigeza@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Manuel Alejandro Quistial Jurado","manuel.quistialj@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("JESSICA CARRILLO RAMIREZ","JECAR316@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("SILDANA MOLINA VEGA","SILDANAMOLINA@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Tatiana Caicedo","tcaicedo29@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Juan Pablo Puerta","jpuertapelaez@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Daniela Sanchez","danisanchez2510@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Carlos Alfredo Lagos Castro","carloslagos228@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Claudia Andrea Ochoa Serna","claudiaag91@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("MONICA BELLA","ingmonicabella@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Zaida milena Rodríguez cortés","Zaidarodriguez23@hotmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("VALENTINA ESQUIVEL BUSTOS","ESQUI.BELL1@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("LORENA URUEÑA","LOURUENA@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("FABIAN ANDRES BELTRAN CORTES","FABC07@GMAIL.COM","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Catalina Maria Velasquez Gutiérrez","Katyvelasquez@gmail.com","TRANSFORMACION VIRTUAL","19/02/2020"),
        array("Harold Steven Arango gomez","Harold.arango41@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Daniela parra cantor","Daniela.parrita@hotmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Liliana López Becerra","liliana.Lopez.becerra85@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Laura Catalina Céspedes Sánchez","laura.cespedes22@hotmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Juan Sebastian Guaman Rojas","jsebatiangr@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Juan Manuel Bohorquez","juanbohorquez373@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Carolina Salazar Gómez","Ascarosalazar10@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Eliana Lizcano Tobón","elifase4@gmail.com","TRANSFORMACION VIRTUAL","02/03/2020"),
        array("Laura Rueda","laura.rueda098@gmail.com","TRANSFORMACION VIRTUAL","13/10/2022")
    );
    for ($i=0; $i < count($newUser); $i++) { 
        $user = get_user_by( 'email',$newUser[$i][1] );
        if($user === false){
            wp_create_user( $newUser[$i][0] ,"123456789",$newUser[$i][1] );
            $user = get_user_by( 'email',$newUser[$i][1] );
            update_user_meta( $user->data->ID, 'suscripcion' , "yes");
            update_user_meta( $user->data->ID, 'fechasuscripcion', $newUser[$i][3] );
        }else{
            update_user_meta( $user->data->ID, 'suscripcion' , "yes");
            update_user_meta( $user->data->ID, 'fechasuscripcion', $newUser[$i][3] );
        }
    }
}
//add_shortcode('CRNAD_createNewUsers', 'CRNAD_createNewUsers');