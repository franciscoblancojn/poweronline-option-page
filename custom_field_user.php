<?php
add_action( 'show_user_profile', 'CRNAD_custom_field_user' );
add_action( 'edit_user_profile', 'CRNAD_custom_field_user' );

function CRNAD_custom_field_user( $user ) { 
    $sus = get_user_meta( $user->ID, 'suscripcion' , true);
    $fechaUser = get_user_meta( $user->ID, 'fechasuscripcion', true );
    ?>
    <h3>
        <?=_("Suscipcion User"); ?>
    </h3>
    <table class="form-table">
        <tr>
            <th>
                <label for="suscripcion">
                    <?=_("Suscripcion")?>
                </label>
            </th>
            <td>
                <input 
                type="checkbox" 
                name="suscripcion" 
                id="suscripcion" 
                class="regular-text" 
                <?php
                    if($sus == "yes"){
                        echo "checked";
                    }
                ?>
                />
                <label for="suscripcion" class="description">
                    <?=_("Habilite el Checkbox si el usuario posee suscripcion")?>
                </label>
            </td>
        </tr>
        <tr>
            <th>
                <label for="fechasuscripcion">
                    <?=_("Fecha")?>
                </label>
            </th>
            <td>
                <input 
                type="date" 
                name="fechasuscripcion" 
                id="fechasuscripcion" 
                class="regular-text" 
                value="<?=$fechaUser?>"
                />
                <label for="fechasuscripcion" class="description">
                    <?=_("Fecha final de suscripcion")?>
                </label>
            </td>
        </tr>
    </table>
<?php }
add_action( 'personal_options_update', 'CRNAD_custom_field_user_save' );
add_action( 'edit_user_profile_update', 'CRNAD_custom_field_user_save' );

function CRNAD_custom_field_user_save( $user_id ) {
    update_user_meta( $user_id, 'suscripcion', ($_POST['suscripcion']=="on")?"yes":"no" );
    update_user_meta( $user_id, 'fechasuscripcion', $_POST['fechasuscripcion'] );
}

