<?php
function CRNAD_plugin_activation() {
    if( ! wp_next_scheduled( 'cron_hook_CRNAD' ) ) {
        wp_schedule_event( current_time( 'timestamp' ), 'daily', 'cron_hook_CRNAD' );
    }
}
add_action( 'cron_hook_CRNAD', 'function_cron_hook_CRNAD' );
function function_cron_hook_CRNAD() {
    CRNAD_users_order_recurrent();
}
function CRNAD_get_all_id_users(){
    $users = get_users();
    $users = array_map(function($e){
        return $e->data->ID;
    },$users);
    return $users;
}
function CRNAD_users_order_recurrent(){
    $users = CRNAD_get_all_id_users();
    for ($i=0; $i < count($users); $i++) { 
        CRNAD_finalSuscripcion($users[$i]);
    }
}
function CRNAD_finalSuscripcion($user_id){
    $suscripcion = get_user_meta( $user_id, 'suscripcion', true );
    if($suscripcion == "yes"){
        $fechaUser = get_user_meta( $user_id, 'fechasuscripcion', true );

        $fechaUser = DateTime::createFromFormat("Y-m-d", $fechaUser);
        if(CRNAD_isFechaFinalSuscripcion($fechaUser)){
            update_user_meta( $user_id, 'suscripcion', "no" );
        }
    }
}
function CRNAD_isFechaFinalSuscripcion($fechaUser){
    date_default_timezone_set('America/Bogota');
    return (
        (
            intval($fechaUser->format("Y")) <= intval(date("Y")) &&
            intval($fechaUser->format("d")) <= intval(date("d"))
        )
        ||
        (
            intval($fechaUser->format("Y")) == intval(date("Y")) &&
            intval($fechaUser->format("m")) <= intval(date("m")) &&
            intval($fechaUser->format("d")) <= intval(date("d"))
        )
    );
}
// function testingC() {
//     CRNAD_finalSuscripcion(435);
// }
// add_shortcode( 'testingC', 'testingC' );