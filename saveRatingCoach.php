<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');
header('Content-Type: application/json; charset=utf-8');

date_default_timezone_set("America/Bogota");
$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}

try {
    
    if(empty($_POST["coach"])){
        echo json_encode(array(
            "type" => "error",
            "msj" => "Coach void"
        ));
        exit;
    }


    if(empty($_POST["user_id"])){
        echo json_encode(array(
            "type" => "error",
            "msj" => "user_id void"
        ));
        exit;
    }


    if(empty($_POST["puntuatio"])){
        echo json_encode(array(
            "type" => "error",
            "msj" => "puntuatio void"
        ));
        exit;
    }


    $data = get_option("CRNAD_Coach_Rating");
    if($data == null || $data == "" || $data == undefine){
        $data = "[]";
    }
    $data = json_decode($data,true);

    $preData = $data;

    if($preData[$_POST["coach"]]["users"] == null){
        $preData[$_POST["coach"]]["users"] = [];
    }

    $preData[$_POST["coach"]]["users"]["user_id_".$_POST["user_id"]] = floatval($_POST["puntuatio"]);

    $total = 0;
    foreach ($preData[$_POST["coach"]]["users"] as $key => $value) {
        $total+=$value;
    }

    $data[$_POST["coach"]] = array(
        'coach'     => $_POST["coach"],
        "users"     => $preData[$_POST["coach"]]["users"],
        "rating"    => $total / count($preData[$_POST["coach"]]["users"]),
    );

    update_option("CRNAD_Coach_Rating",json_encode($data));

    echo json_encode(array(
        "type" => "ok",
        "msj" => "puntuation save"
    ));
    exit;
} catch (\Throwable $th) {
    echo json_encode(array(
        "type" => "error",
        "msj" => $th,
        "error" => $th
    ));
    exit;
}