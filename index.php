<?php
/*
Plugin Name: CronogramaAdmin
Plugin URI: https://startscoinc.com/es/
Description: Administrador del cronograma de clases
Author: Startsco
Version: 1.0.14
Author URI: https://startscoinc.com/es/#
License: 
*/


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/poweronline-option-page',
	__FILE__,
	'poweronline-option-page'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');



define('CRNAD_url',plugin_dir_url( __FILE__ ));
define('CRNAD_dir',plugin_dir_path( __FILE__ ));

define('CRNAD_minHora',0);
define('CRNAD_maxHora',24);

define('CRNAD_timedown',5);
define('CRNAD_timeup',7);

require_once plugin_dir_path( __FILE__ ) . 'functions.php';
require_once plugin_dir_path( __FILE__ ) . 'page_option.php';
require_once plugin_dir_path( __FILE__ ) . 'shortcode.php';
//require_once plugin_dir_path( __FILE__ ) . 'widget.php';
require_once plugin_dir_path( __FILE__ ) . 'custom_field_user.php';
//require_once plugin_dir_path( __FILE__ ) . 'cron.php';


function CRNAD_OnLogout(){
  wp_redirect( site_url() );
  exit();
}
add_action('wp_logout','CRNAD_OnLogout');


//register_activation_hook( __FILE__, 'CRNAD_plugin_activation' );