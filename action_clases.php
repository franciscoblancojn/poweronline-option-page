<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

date_default_timezone_set("America/Bogota");
$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}
$user_id = get_current_user_id();
if($user_id == null || $user_id == 0){
    $r = array(
        "status"    => "error",
        "typeError" => "noRegister",
        "msj"       => "Desbes ingresar a la plataforma para aceder a las clases"
    );
    echo json_encode($r);
    exit;
}
$sus = get_user_meta( $user_id , 'suscripcion' , true);
if($sus != "yes"){
    $r = array(
        "status"    => "error",
        "typeError" => "noSuscription",
        "msj"       => "Actualmente no cuenta con un plan activo, comunicate con tu asesor"
    );
    echo json_encode($r);
    exit;
}
$coach = $_POST['coach'];
if($coach == null || $coach == ""){
    $r = array(
        "status"    => "error",
        "typeError" => "coachInvalido",
        "msj"       => "El coach es Invalido"
    );
    echo json_encode($r);
    exit;
}
$hora = date("H");
$minutos = date("i");

if($minutos >= (60 - CRNAD_timedown)){
    $minutos = 0;
    $hora++;
}elseif($minutos >= (30 - CRNAD_timedown) && $minutos <= (30 + CRNAD_timeup)){
    $minutos = 30;
}elseif($minutos <= CRNAD_timeup){
    $minutos = 0;
}else{
    $r = array(
        "status"    => "error",
        "typeError" => "timeOut",
        "msj"       => "Paso el tiempo para acceder a la Clase, intentalo de nuevo en la siguiete hora"
    );
    echo json_encode($r);
    exit;
}

$minutos = intval($minutos/30) * 30;
$min_id = $hora.(($minutos == 30)?"_mm":"");
$hora = $_POST['hora'];
if($hora == null || $hora == "" || $hora!=$min_id){
    $r = array(
        "status"    => "error",
        "typeError" => "horaInvalido",
        "msj"       => "La hora es Invalido"
    );
    echo json_encode($r);
    exit;
}
$cronograma = get_option( 'input_CRNAD_option_settings' );
$cronograma = json_decode($cronograma,true);

$dias = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
$dia = $dias[date("N") - 1];

$clases = $cronograma[$dia][$hora];
if($clases == null){
    $r = array(
        "status"    => "error",
        "typeError" => "clase404",
        "msj"       => "No hay clases disponibles a esta hora"
    );
    echo json_encode($r);
    exit;
}
$clases = json_decode($clases,true);
if($clases == null || count($clases) == 0){
    $r = array(
        "status"    => "error",
        "typeError" => "clase404",
        "msj"       => "No hay clases disponibles a esta hora"
    );
    echo json_encode($r);
    exit;
}

$clase = null;
for ($i=0; $i < count($clases); $i++) { 
    if($clases[$i]["coach"] == $coach){
        $clase = $clases[$i];
    }
}
if($clase == null){
    $r = array(
        "status"    => "error",
        "typeError" => "coach404",
        "msj"       => "No hay clases disponibles con este coach a esta hora"
    );
    echo json_encode($r);
    exit;
}


///OK
$r = array(
    "status"    => "ok",
    "typeError" => "ok",
    "msj"       => "Clase encontrada",
    "data"      => $clase
);
echo json_encode($r);
exit;

